# Cardano Mainnet on MANTRA DAO

## Submodules

- [input-output-hk/cardano-node](https://github.com/input-output-hk/cardano-node) @ `1.33.0`
- [input-output-hk/libsodium](https://github.com/input-output-hk/libsodium) @ `66f017f1`
